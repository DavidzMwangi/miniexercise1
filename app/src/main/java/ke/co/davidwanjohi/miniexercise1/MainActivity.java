package ke.co.davidwanjohi.miniexercise1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button screenOne, screenTwo,screenThree;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        screenOne=findViewById(R.id.screen_one);
        screenTwo=findViewById(R.id.screen_two);
        screenThree=findViewById(R.id.screen_three);


        screenOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ScreenOneActivity.class);
                startActivity(intent);
            }
        });


        screenTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this,ScreenTwoActivity.class);
                startActivity(intent);
            }
        });

        screenThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this,ScreenThreeActivity.class);
                startActivity(intent);
            }
        });
    }
}
